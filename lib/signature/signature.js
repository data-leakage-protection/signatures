const assignHashPropertyTo = require('./mixins/assign-hash-property-to')
const assignNamePropertyTo = require('./mixins/assign-name-property-to')
const jsonRe2Replacer = require('./json-re2-replacer')
const match = require('./match')
const nullSignature = require('./null-signature')
const toRe2 = require('./to-re2')

class Signature {
  constructor (params = nullSignature) {
    const { caption, description, part, pattern, type } = params
    this.caption = caption
    this.description = description
    this.hash = null
    assignNamePropertyTo(this)
    this.part = part
    this.pattern = toRe2({
      pattern,
      type
    })
    this.type = type

    // This property definition MUST always be last

    assignHashPropertyTo(this)
  }

  match (value) {
    return match(value, this)
  }

  toJson () {
    return JSON.stringify(this, jsonRe2Replacer)
  }

  toString () {
    return this.toJson()
  }

  valueOf () {
    return this.pattern.toString()
  }
}

module.exports = Signature
