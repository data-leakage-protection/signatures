const dataLossSignatures = require('..')
const signature = require('../signature')

describe('dataLossSignatures', () => {
  describe('.Signature', () => {
    it('constructs Signature.prototypes', () => {
      expect(dataLossSignatures.Signature).toBe(signature.Signature)
      const defn = new dataLossSignatures.Signature()
      expect(defn).toBeInstanceOf(signature.Signature)
    })
  })
  describe('.factory', () => {
    it('creates a Signature.prototype', () => {
      const defn = dataLossSignatures.factory()
      expect(defn).toBeInstanceOf(signature.Signature)
      // expect(defn).toEqual(signature.nullSignature)
    })
  })
  describe('.nullSignature', () => {
    it('is a NullObject/default', () => {
      expect(signature.nullSignature).toBeDefined()
      const defn = dataLossSignatures.factory()
      expect(defn).toEqual(signature.nullSignature)
    })
  })
})
