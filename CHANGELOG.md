# Changelog

## [1.2.4](https://gitlab.com/data-leakage-protection/signatures/compare/v1.2.3...v1.2.4) (2020-04-15)


### Bug Fixes

* **ci:** use new docker image ([cdca261](https://gitlab.com/data-leakage-protection/signatures/commit/cdca261945f506f67058285ffd55f8e4f7e24a98)), closes [#13](https://gitlab.com/data-leakage-protection/signatures/issues/13)
* **release:** add node version variable ([ed9ef63](https://gitlab.com/data-leakage-protection/signatures/commit/ed9ef63acd593df27153f5ec0303af79938386e4)), closes [#13](https://gitlab.com/data-leakage-protection/signatures/issues/13)
* **release:** update node version for automated releases ([534ba6e](https://gitlab.com/data-leakage-protection/signatures/commit/534ba6e604ad17b4752f4fe6e69df63cfca5eba5)), closes [#13](https://gitlab.com/data-leakage-protection/signatures/issues/13)

## [1.2.3](https://gitlab.com/data-leakage-protection/signatures/compare/v1.2.2...v1.2.3) (2019-10-10)


### Bug Fixes

* **leakage:** remove internal/proprietary endpoints ([1c32340](https://gitlab.com/data-leakage-protection/signatures/commit/1c3234050b5ce5588437ece3b85a1ea40e06ef20)), closes [#11](https://gitlab.com/data-leakage-protection/signatures/issues/11)

## [1.2.2](https://gitlab.com/data-leakage-protection/signatures/compare/v1.2.1...v1.2.2) (2019-10-10)


### Bug Fixes

* **audit:** fix issues reported by `npm audit --json` ([d42ce74](https://gitlab.com/data-leakage-protection/signatures/commit/d42ce74e38c2129376955c5d46e18fdd52f2e20c)), closes [#12](https://gitlab.com/data-leakage-protection/signatures/issues/12)

## [1.2.1](https://gitlab.com/data-leakage-protection/signatures/compare/v1.2.0...v1.2.1) (2019-08-12)

# [1.2.0](https://gitlab.com/data-leakage-protection/signatures/compare/v1.1.1...v1.2.0) (2019-06-18)


### Features

* **signature:** add name and hash props ([af3168b](https://gitlab.com/data-leakage-protection/signatures/commit/af3168b)), closes [#2](https://gitlab.com/data-leakage-protection/signatures/issues/2)
* **signature:** add props for filtering ([b35d89b](https://gitlab.com/data-leakage-protection/signatures/commit/b35d89b)), closes [#2](https://gitlab.com/data-leakage-protection/signatures/issues/2)

## [1.1.1](https://gitlab.com/data-leakage-protection/signatures/compare/v1.1.0...v1.1.1) (2019-06-14)

# [1.1.0](https://gitlab.com/data-leakage-protection/signatures/compare/v1.0.5...v1.1.0) (2019-06-14)


### Bug Fixes

* **re2:** serialize regex strings ([a3767ae](https://gitlab.com/data-leakage-protection/signatures/commit/a3767ae)), closes [#4](https://gitlab.com/data-leakage-protection/signatures/issues/4)
* **signature:** ensure RE2 parses regular expression string representations ([93f56f5](https://gitlab.com/data-leakage-protection/signatures/commit/93f56f5)), closes [#4](https://gitlab.com/data-leakage-protection/signatures/issues/4)
* **signature:** handle regex strings ([5c91a3b](https://gitlab.com/data-leakage-protection/signatures/commit/5c91a3b)), closes [#4](https://gitlab.com/data-leakage-protection/signatures/issues/4)
* **signature:** handle regex strings ([843000c](https://gitlab.com/data-leakage-protection/signatures/commit/843000c)), closes [#4](https://gitlab.com/data-leakage-protection/signatures/issues/4)


### Code Refactoring

* **module:** rename module ([8026d57](https://gitlab.com/data-leakage-protection/signatures/commit/8026d57)), closes [#1](https://gitlab.com/data-leakage-protection/signatures/issues/1)
* **module:** rename module ([e52ebee](https://gitlab.com/data-leakage-protection/signatures/commit/e52ebee))


### Features

* **contents:** add 18 signatures ([52bc896](https://gitlab.com/data-leakage-protection/signatures/commit/52bc896)), closes [#1](https://gitlab.com/data-leakage-protection/signatures/issues/1)


### BREAKING CHANGES

* **module:** rename module with scope '@data-leakage-protection'.

Use the scope '@data-leakage-protection' for this and future, related
modules to more easily identify DLDP features.

See merge request data-leakage-protection/signatures!1
* **module:** rename module with scope '@data-leakage-protection'.

Use the scope '@data-leakage-protection' for this and future, related
modules to more easily identify DLDP features.

# 1.0.0 (2019-03-20)


### Bug Fixes

* **dependencies:** update ([4ecfa4f](https://gitlab.com/data-leakage-protection/signatures/commit/4ecfa4f)), closes [#1](https://gitlab.com/data-leakage-protection/signatures/issues/1)
* **javascript-S930:** no extra fn args ([be8832e](https://gitlab.com/data-leakage-protection/signatures/commit/be8832e)), closes [#1](https://gitlab.com/data-leakage-protection/signatures/issues/1)
* **release:** bundle before running semantic-release ([8b9c693](https://gitlab.com/data-leakage-protection/signatures/commit/8b9c693)), closes [#8](https://gitlab.com/data-leakage-protection/signatures/issues/8)
* **release:** bundle before running semantic-release ([339165a](https://gitlab.com/data-leakage-protection/signatures/commit/339165a)), closes [#8](https://gitlab.com/data-leakage-protection/signatures/issues/8)
* **release:** bundle release packages ([57b35f2](https://gitlab.com/data-leakage-protection/signatures/commit/57b35f2)), closes [#4](https://gitlab.com/data-leakage-protection/signatures/issues/4)
* **release:** minify bundle and exclude package.json ([dae999c](https://gitlab.com/data-leakage-protection/signatures/commit/dae999c)), closes [#8](https://gitlab.com/data-leakage-protection/signatures/issues/8)
* **release:** patch v1.0.0 ([e7b40bd](https://gitlab.com/data-leakage-protection/signatures/commit/e7b40bd)), closes [#3](https://gitlab.com/data-leakage-protection/signatures/issues/3)
* **release:** patch v1.0.0 ([f19f3f0](https://gitlab.com/data-leakage-protection/signatures/commit/f19f3f0)), closes [#3](https://gitlab.com/data-leakage-protection/signatures/issues/3)


### Code Refactoring

* **module:** rename module ([8026d57](https://gitlab.com/data-leakage-protection/signatures/commit/8026d57)), closes [#1](https://gitlab.com/data-leakage-protection/signatures/issues/1)
* **module:** rename module ([e52ebee](https://gitlab.com/data-leakage-protection/signatures/commit/e52ebee))


### Features

* **api:** get signatures w/gitlab REST api ([2190fa2](https://gitlab.com/data-leakage-protection/signatures/commit/2190fa2)), closes [#1](https://gitlab.com/data-leakage-protection/signatures/issues/1)
* **push-rules:** gitlab dldp ([25b7da4](https://gitlab.com/data-leakage-protection/signatures/commit/25b7da4)), closes [#1](https://gitlab.com/data-leakage-protection/signatures/issues/1)
* **security:** adopt data-leakage detection data model ([a47a8cc](https://gitlab.com/data-leakage-protection/signatures/commit/a47a8cc)), closes [#1](https://gitlab.com/data-leakage-protection/signatures/issues/1)


### BREAKING CHANGES

* **module:** rename module with scope '@data-leakage-protection'.

Use the scope '@data-leakage-protection' for this and future, related
modules to more easily identify DLDP features.

See merge request data-leakage-protection/signatures!1
* **module:** rename module with scope '@data-leakage-protection'.

Use the scope '@data-leakage-protection' for this and future, related
modules to more easily identify DLDP features.

## [:package: v1.0.5](https://gitlab.com/data-leakage-protection/signatures/compare/v1.0.4...v1.0.5) (2019-03-06)

### Fixes

* **release:** minify bundle and exclude package.json ([dae999c](https://gitlab.com/data-leakage-protection/signatures/commit/dae999c)), closes [#8](https://gitlab.com/data-leakage-protection/signatures/issues/8)

## [:package: v1.0.4](https://gitlab.com/data-leakage-protection/signatures/compare/v1.0.3...v1.0.4) (2019-03-04)

---

## [:package: v1.0.3](https://gitlab.com/data-leakage-protection/signatures/compare/v1.0.2...v1.0.3) (2019-03-04)

> ![Closed issue][octicon-issue-closed] Closes issue [#8](https://gitlab.com/data-leakage-protection/signatures/issues/8)

### Fixes

**Bundle distribution on the `postversion` npm-run-script hook during semantic-release**. <sup>([8b9c693](https://gitlab.com/data-leakage-protection/signatures/commit/8b9c693)), ([339165a](https://gitlab.com/data-leakage-protection/signatures/commit/339165a))</sup>

-   _Summary:_

    > Execute `npm run bundle` during the `postversion` hook.

-  _Reason(s) for change:_

    > @data-leakage-protection/signatures-vX.Y.Z.tgz packages _still_ publish without the dist/@data-leakage-protection/signatures.js module.

---

## [:package: v1.0.2](https://gitlab.com/data-leakage-protection/signatures/compare/v1.0.1...v1.0.2) <time datetime="2019-03-01">(2019-03-01)</time>
> ![Closed issue][octicon-issue-closed] Closes issue [#4: "fix(release): npm package will not install"](https://gitlab.com/data-leakage-protection/signatures/issues/4).

### Fix

**Automate each release with bundled, minified distributions**. <sup>([57b35f2](https://gitlab.com/data-leakage-protection/signatures/commit/57b35f2))</sup>

-   _Summary:_

    > Use **[rollup.js ![docs][octicon-link-external]](https://rollupjs.org/)** to **remove all non-essential dependencies**, so we can distribute a package that **installs more quickly** with a **smaller surface attack area.**

-   _Reason(s) for change:_

    The **npm-run-script** `build` now creates a package that is seven-times (7X) smaller:

    > ```bash
    > npm pack
    > npm notice
    > npm notice 📦  @data-leakage-protection/signatures@1.0.1
    > npm notice === Tarball Contents ===
    > npm notice 9.7kB  package.json
    > npm notice 2.1kB  CHANGELOG.md
    > npm notice 11.4kB LICENSE
    > npm notice 48.1kB README.md
    > npm notice 34.1kB dist/@data-leakage-protection/signatures.js
    > npm notice === Tarball Details ===
    > npm notice name:          @data-leakage-protection/signatures
    > npm notice version:       1.0.1
    > npm notice filename:      @data-leakage-protection/signatures-1.0.1.tgz
    > npm notice package size:  23.4 kB
    > npm notice unpacked size: 105.2 kB
    > npm notice shasum:        c63d08dadd60060dc654392276532988b87406fd
    > npm notice integrity:     sha512-p+KUMsbedpg8L[...]0yrt0a0/UuV8g==
    > npm notice total files:   5
    > npm notice
    > @data-leakage-protection/signatures-1.0.1.tgz
    > ```

---

## [:package: v1.0.1](https://gitlab.com/data-leakage-protection/signatures/compare/v1.0.0...v1.0.1) (2019-02-28)

> ![Closed issue][octicon-issue-closed] Closes issue [#3: "fix(release): revise typos and inaccuracies"](https://gitlab.com/data-leakage-protection/signatures/issues/3).

### Fix

1. **Address installation errors**. <sup>([e7b40bd](https://gitlab.com/data-leakage-protection/signatures/commit/e7b40bd)), ([f19f3f0](https://gitlab.com/data-leakage-protection/signatures/commit/f19f3f0))</sup>

    _Summary:_

    > Ensure the module can install without errors when using `npm install` or `yarn add`.

    _Reason(s) for change:_

    > Consumers cannot use the module if they cannot load it.

---

# Publically Available

## :package: v1.0.0 <time datetime="2019-02-27">(2019-02-27)</time>

> ![Closed issue][octicon-issue-closed] Closes issue [#1: "feat(security): create an actionable data-model for detecting data-leakage"](https://gitlab.com/data-leakage-protection/signatures/issues/1).

### Features

1.  **Use an RE2 expression engine to match file system resources against common date-leakage patterns.** <sup>([25b7da4](https://gitlab.com/data-leakage-protection/signatures/commit/25b7da4))</sup>

    _Summary:_

    > When constructing a `Signature` object whose `part` property has a value of "extension", "filename", or "path", convert the `Singature's` `pattern` to an [RE2 expresion ![docs][octicon-link-external]](https://github.com/google/re2/wiki/Syntax) (instead of a JavaScript regular expression) with **[node-re2 ![node-re2's README on GitHub][octicon-link-external]](https://github.com/uhop/node-re2#readme)**.

    _Reason(s) for change:_

    >
    > - **Security**: RE2 syntax and engines are more secure than PECL and Perl-like regular expression engines. [^1]
    ><br>
    > - **Golang**: RE2 is the default syntax for Golang. [^2]
    ><br>
    > - **GitLab Push Rules use Golang**: GitLab uses Golang to evaluate file patterns for possible data-leakage with a pre-receive hook. [^3]

1. **Use `Signatures` to model and detect (simple) data-leakage patterns.** <sup>([a47a8cc](https://gitlab.com/data-leakage-protection/signatures/commit/a47a8cc))</sup>

    _Summary:_

    > Adopt a simple data-leakage detection data model.

    _Reason(s) for change:_

    > A normalized data-model the can define and identify data leakage streamlines communication and enables testing.

### Fixes

1.  **Upgrade outdated dependencies.** <sup>([4ecfa4f](https://gitlab.com/data-leakage-protection/signatures/commit/4ecfa4f))</sup>

    _Summary:_

    > Resolve dependency drift.

    _Reason(s) for change:_

    > Keeping third-party dependencies up-to-date mitigates security risks. 

1.  **Deodorize source code: fix "code smells".** <sup>([be8832e](https://gitlab.com/data-leakage-protection/signatures/commit/be8832e))</sup>

    _Summary:_

    > Remove unused arguments from a single function.

    _Reason(s) for change:_

    > Unneccessary arguments contributes to cognitive complexity.

---

## About this Changelog

All notable changes to **@data-leakage-protection/signatures** will be documented in this file.

## Format

The format is loosely based on [Keep a Changelog ![external resource][octicon-link-external]](https://keepachangelog.com/en/1.0.0/).

## Semantic versions

All production releases adhere to the
[Semantic Versioning specification ![external resource][octicon-link-external]](https://semver.org/spec/v2.0.0.html).

## References

[^1]: _google/re2._ (2019). _GitHub_. Retrieved 1 March 2019, from <https://github.com/google/re2/wiki/WhyRE2>

[^2]: _regexp - The Go Programming Language_. (2019). _Golang.org_. Retrieved 1 March 2019, from <https://golang.org/pkg/regexp/#pkg-overview>

[^3]: _Push Rules | GitLab_. (2019). _Docs.gitlab.com_. Retrieved 1 March 2019, from <https://docs.gitlab.com/ee/push_rules/push_rules.html>

<!-- ⛔️ Do not remove this comment or anything under it ⛔️ -->

<!-- 🔗  label link references 🔗   -->

[octicon-alert]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/alert.svg

[octicon-issue-closed]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/issue-closed.svg

[octicon-git-commit]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/git-commit.svg

[octicon-link-external]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/link-external.svg

[octicon-tag]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/tag.svg
